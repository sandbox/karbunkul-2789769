<?php

namespace Drupal\ra_client;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for ra_client managers.
 */
interface RaClientManagerInterface extends PluginManagerInterface {

}
