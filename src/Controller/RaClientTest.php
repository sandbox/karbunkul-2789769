<?php

namespace Drupal\ra_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ra_client\RaClient;

/**
 * Class RaClientTest.
 *
 * @package Drupal\ra_client\Controller
 */
class RaClientTest extends ControllerBase {

  /**
   * Main.
   *
   * @return string
   *   Return available methods from http://emerap.com.
   */
  public function main() {
    $client = new RaClient('emerap');
    $methods = $client->query('ra.methods')->getRaw();
    return [
      '#type' => 'markup',
      '#markup' => $methods,
    ];
  }

}
