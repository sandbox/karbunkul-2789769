<?php

namespace Drupal\ra_client;

use Emerap\RaClient\Client;

/**
 * Class RaClient.
 *
 * @package Drupal\ra_client
 */
class RaClient extends Client {

  /**
   * {@inheritdoc}
   */
  static public function getSources() {
    /** @var \Drupal\ra_client\RaClientManager $manager */
    $manager = \Drupal::service('plugin.manager.ra_client');
    return $manager->getDefinitions();
  }

}
