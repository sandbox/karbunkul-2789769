# Ra Framework client API for Drupal 8

## Synopsis

Provides Ra Framework client API. 

Implementation emerap/ra_client library for Drupal 8.

## ra.client.yml file

Create file [MODULE_NAME].ra.client.yml inside module directory.

### YAML example.

```
source_id:
    path: 'http://emerap.com/ra/method'
```
#### Php example

```
use Drupal\ra_client\RaClient;

...

// \Drupal\ra_client\RaClient instance
$client = new RaClient('emerap');

// Get all available methods.
$methods = $client->query('ra.methods')->getRaw();
```
